import { SET_CURRENT_USER } from '../actions/types';

const initialState = {
    validJsonWebToken: false,
    user: {}
};

const booleanActionPayload = (payload) => {
    if (payload) {
        return true;
    } else {
        return false;
    }
};

export default function(state = initialState, action) {
    switch(action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                validJsonWebToken: booleanActionPayload(action.payload),
                user: action.payload
            };
        default:
            return state;
    }
};
