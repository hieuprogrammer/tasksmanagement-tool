import { combineReducers } from 'redux';
import errorsReducer from './errorsReducer';
import projectsReducer from './projectsReducer';
import backlogsReducer from './backlogsReducer';
import securityReducer from './securityReducer';

export default combineReducers({
    errors: errorsReducer,
    project: projectsReducer,
    backlog: backlogsReducer,
    security: securityReducer
});