import axios from 'axios';

const setJsonWebToken = jsonWebToken => {
    if (jsonWebToken) {
        axios.defaults.headers.common["Authorization"] = jsonWebToken;
    } else {
        delete axios.defaults.headers.common["Authorization"];
    }
};

export default setJsonWebToken;
