import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Dashboard from "./components/Dashboard";
import Header from "./components/Layout/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AddProject from "./components/Projects/AddProject";
import { Provider } from 'react-redux';
import store from "./store";
import UpdateProject from "./components/Projects/UpdateProject";
import ProjectBoard from "./components/ProjectBoard/ProjectBoard";
import AddTask from "./components/ProjectBoard/Tasks/AddTask";
import UpdateTask from "./components/ProjectBoard/Tasks/UpdateTask";
import Landing from "./components/Layout/Landing";
import Register from "./components/UserManagement/Register";
import Login from "./components/UserManagement/Login";
import jwt_decode from 'jwt-decode';
import setJsonWebToken from './securityUtils/setJsonWebToken';
import { SET_CURRENT_USER } from './actions/types';
import { logout } from './actions/securityActions';
import SecuredRoute from './securityUtils/SecuredRoute';

const jsonWebToken = localStorage.jsonWebToken;

if (jsonWebToken) {
  setJsonWebToken(jsonWebToken);
  const decodedJsonWebToken = jwt_decode(jsonWebToken);
  store.dispatch({
    type: SET_CURRENT_USER,
    payload: decodedJsonWebToken
  });

  const currentTime = Date.now() / 1000;

  if (decodedJsonWebToken.exp < currentTime) {
    store.dispatch(logout());
    window.location.href = "/";
  }
}

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Header/>
          {
            // Public routes:
          }
          <Route exact path="/" component={Landing}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          {
            // Private routes:
          }
          <Switch>
            <SecuredRoute exact path="/dashboard" component={Dashboard}/>
            <SecuredRoute exact path="/add-project" component={AddProject}/>
            <SecuredRoute exact path="/update-project/:id" component={UpdateProject}/>
            <SecuredRoute exact path="/project-board/:id" component={ProjectBoard}/>
            <SecuredRoute exact path="/add-task/:id" component={AddTask}/>
            <SecuredRoute exact path="/update-task/:backlog_id/:task_id" component={UpdateTask}/>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
