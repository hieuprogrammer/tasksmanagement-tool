import axios from 'axios';
import { 
    GET_ERRORS, 
    SET_CURRENT_USER 
} from './types';
import setJsonWebToken from '../securityUtils/setJsonWebToken';
import jwt_decode from 'jwt-decode';

export const createNewUser = (newUser, history) => async dispatch => {
    try {
        await axios.post(`/api/users/register`, newUser);
        history.push('/login');
        dispatch({
            type: GET_ERRORS,
            payload: {}
        });
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }
};

export const login = LoginRequest => async dispatch => {
    try {
        // End user send HTTP 1.1 POST request to log in:
        const response = await axios.post(`/api/users/login`, LoginRequest);
        // Extract JSON Web Token from response.data:
        const { jsonWebToken } = response.data;
        // Store JSON Web Token in localStorage:
        localStorage.setItem('jsonWebToken', jsonWebToken);
        // Set JSON Web Token in HTTP Header:
        setJsonWebToken(jsonWebToken);
        // Decode JWT on ReactJS frontend client:
        const decodedJsonWebToken = jwt_decode(jsonWebToken);
        // Dispatch to Security Reducer:
        dispatch({
            type: SET_CURRENT_USER,
            payload: decodedJsonWebToken
        });
    } catch (error) {
        dispatch({
            type: GET_ERRORS,
            payload: error.response.data
        });
    }
};

export const logout = () => dispatch => {
    localStorage.removeItem('jsonWebToken');
    setJsonWebToken(false);
    dispatch({
        type: SET_CURRENT_USER,
        payload: {}
    });
};
