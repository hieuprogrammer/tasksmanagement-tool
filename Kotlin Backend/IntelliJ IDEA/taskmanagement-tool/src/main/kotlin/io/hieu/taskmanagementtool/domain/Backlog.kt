package io.hieu.taskmanagementtool.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
class Backlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
    var projectTaskSequence = 0
    var projectIdentifier: String? = null

    // JPA's OneToOne relationship mapping with Project entity:
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonIgnore // Adding this @JsonIgnore annotation on the child side of the JPA relationship will solve the recursive issue which would lead to a Stack Overflow error.
    var project: Project? = null

    // JPA's OneToMany relationship mapping with Task entity:
    @OneToMany(cascade = [CascadeType.REFRESH], fetch = FetchType.EAGER, mappedBy = "backlog", orphanRemoval = true)
    var tasks: List<Task> = ArrayList<Task>()

}