package io.hieu.taskmanagementtool.services.impl

import io.hieu.taskmanagementtool.services.ErrorValidationService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult

@Service
class ErrorValidationServiceImpl : ErrorValidationService {
    override fun ErrorValidationService(bindingResult: BindingResult?): ResponseEntity<*>? {
        return if (bindingResult!!.hasErrors()) {
            val errors: MutableMap<String, String?> = HashMap()
            for (error in bindingResult.fieldErrors) {
                errors[error.field] = error.defaultMessage
            }
            ResponseEntity<Map<String, String?>>(errors, HttpStatus.BAD_REQUEST)
        } else {
            null
        }
    }
}