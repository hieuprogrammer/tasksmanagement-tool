package io.hieu.taskmanagementtool.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null
    var projectName: @NotBlank(message = "Project name is required!") String? = null

    @Column(updatable = false, unique = true)
    var projectIdentifier: @NotBlank(message = "Project identifier is required!") @Size(min = 4, max = 5, message = "Project identifier must be between 4 to 5 characters long!") String? = null
    var description: @NotBlank(message = "Project description is required!") String? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var startDate: Date? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var endDate: Date? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var created_At: Date? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var updated_At: Date? = null

    // JPA's OneToOne relationship mapping with Backlog entity:
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], mappedBy = "project")
    @JsonIgnore
    var backlog: Backlog? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    var user: User? = null
    var projectLeader: String? = null

    @PrePersist
    protected fun onCreate() {
        created_At = Date()
    }

    @PreUpdate
    protected fun onUpdate() {
        updated_At = Date()
    }
}