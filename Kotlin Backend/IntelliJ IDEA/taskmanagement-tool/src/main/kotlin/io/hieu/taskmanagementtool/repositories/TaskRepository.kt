package io.hieu.taskmanagementtool.repositories

import io.hieu.taskmanagementtool.domain.Task
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TaskRepository : CrudRepository<Task?, Long?> {
    fun findByProjectIdentifierOrderByPriority(id: String?): List<Task?>?

    fun findByProjectSequence(sequence: String?): Task?
}