package io.hieu.taskmanagementtool.security

import com.google.gson.Gson
import io.hieu.taskmanagementtool.exceptions.InvalidLoginResponse
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JsonWebTokenAuthenticationEntryPoint : AuthenticationEntryPoint {
    @Throws(IOException::class, ServletException::class)
    override fun commence(
            httpServletRequest: HttpServletRequest,
            httpServletResponse: HttpServletResponse,
            e: AuthenticationException
    ) {
        val loginResponse = InvalidLoginResponse()
        val jsonLoginResponse = Gson().toJson(loginResponse)
        httpServletResponse.contentType = "application/json"
        httpServletResponse.status = 401
        httpServletResponse.writer.print(jsonLoginResponse)
    }
}