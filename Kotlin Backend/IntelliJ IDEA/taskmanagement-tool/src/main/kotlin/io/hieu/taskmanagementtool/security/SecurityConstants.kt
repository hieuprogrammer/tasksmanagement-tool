package io.hieu.taskmanagementtool.security

object SecurityConstants {
    const val SIGN_UP_URLS = "/api/users/**"
    const val H2_URL = "h2-console/**"
    const val JWT_SECRET_KEY = "SecretKeyToGenerateJWTs"
    const val JWT_PREFIX = "Bearer "
    const val HTTP_HEADER_STRING = "Authorization"
    const val JWT_EXPIRATION_TIME: Long = 30000 // 30 seconds.
}