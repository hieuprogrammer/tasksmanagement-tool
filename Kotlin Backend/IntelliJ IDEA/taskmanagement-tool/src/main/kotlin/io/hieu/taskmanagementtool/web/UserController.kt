package io.hieu.taskmanagementtool.web

import io.hieu.taskmanagementtool.domain.User
import io.hieu.taskmanagementtool.payload.LoginRequest
import io.hieu.taskmanagementtool.payload.LoginSuccessJWTResponse
import io.hieu.taskmanagementtool.security.JsonWebTokenProvider
import io.hieu.taskmanagementtool.security.SecurityConstants.JWT_PREFIX
import io.hieu.taskmanagementtool.security.validator.UserValidator
import io.hieu.taskmanagementtool.services.ErrorValidationService
import io.hieu.taskmanagementtool.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping(path = ["/api/users", "api/users.html", "api/users/index", "api/users/index.html"])
class UserController {
    @Autowired
    private val errorValidationService: ErrorValidationService? = null

    @Autowired
    private val userService: UserService? = null

    @Autowired
    private val userValidator: UserValidator? = null

    @Autowired
    private val jsonWebTokenProvider: JsonWebTokenProvider? = null

    @Autowired
    private val authenticationManager: AuthenticationManager? = null

    @PostMapping(path = ["/login", "/login.html", "/login/", "/login/index", "/login/index.html"])
    fun authenticateUser(@RequestBody loginRequest: @Valid LoginRequest?, bindingResult: BindingResult?): ResponseEntity<*> {
        val errors = errorValidationService!!.ErrorValidationService(bindingResult)
        if (errors != null) {
            return errors
        }
        val authentication = authenticationManager!!.authenticate(
                UsernamePasswordAuthenticationToken(
                        loginRequest!!.username,
                        loginRequest.password
                )
        )
        SecurityContextHolder.getContext().authentication = authentication
        val jsonWebToken = JWT_PREFIX + jsonWebTokenProvider!!.generateJWT(authentication)
        return ResponseEntity.ok(LoginSuccessJWTResponse(true, jsonWebToken))
    }

    @PostMapping(path = ["/register", "register.html", "/register/", "register/index", "/register/index.html"])
    fun registerUser(@RequestBody user: @Valid User?, bindingResult: BindingResult?): ResponseEntity<*> {
        // Validate the values of password and confirm password fields match:
        userValidator!!.validate(user!!, bindingResult!!)
        val errors = errorValidationService!!.ErrorValidationService(bindingResult)
        if (errors != null) {
            return errors
        }
        val newUser = userService!!.saveUser(user)
        return ResponseEntity(newUser, HttpStatus.CREATED)
    }
}