package io.hieu.taskmanagementtool.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ProjectNotFoundException(message: String?) : RuntimeException(message)