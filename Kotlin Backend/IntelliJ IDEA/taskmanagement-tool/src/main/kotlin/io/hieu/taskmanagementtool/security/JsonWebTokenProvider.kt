package io.hieu.taskmanagementtool.security

import io.hieu.taskmanagementtool.domain.User
import io.hieu.taskmanagementtool.security.SecurityConstants.JWT_EXPIRATION_TIME
import io.hieu.taskmanagementtool.security.SecurityConstants.JWT_SECRET_KEY
import io.jsonwebtoken.*
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*
import kotlin.collections.HashMap

@Component
class JsonWebTokenProvider {
    // Generate the JSON Web Token:
    fun generateJWT(authentication: Authentication): String {
        val user = authentication.principal as User
        val now = Date(System.currentTimeMillis())
        val expiryDate = Date(now.time + JWT_EXPIRATION_TIME)
        val userId = java.lang.Long.toString(user.id!!)
        val claims: MutableMap<String, Any?> = HashMap()
        claims["id"] = java.lang.Long.toString(user.id!!)
        claims["username"] = user.username
        claims["fullName"] = user.fullName
        return Jwts.builder()
                .setSubject(userId)
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET_KEY)
                .compact()
    }

    // Validate the JSON Web Token:
    fun validateJSONWebToken(jsonWebToken: String?): Boolean {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET_KEY).parseClaimsJws(jsonWebToken)
            return true
        } catch (exception: SignatureException) {
            println("Invalid JSON Web Token signature!")
        } catch (exception: MalformedJwtException) {
            println("Invalid JSON Web Token!")
        } catch (exception: ExpiredJwtException) {
            println("Expired JSON Web Token!")
        } catch (exception: UnsupportedJwtException) {
            println("Unsupported JSON Web Token!")
        } catch (exception: IllegalArgumentException) {
            println("JSON Web Token claims string is empty!")
        }
        return false
    }

    // Get user ID from JSON Web Token:
    fun getUserIdFromJsonWebToken(jsonWebToken: String?): Long {
        val claims: Claims = Jwts.parser().setSigningKey(JWT_SECRET_KEY).parseClaimsJws(jsonWebToken).getBody()
        val id = claims["id"] as String?
        return id!!.toLong()
    }
}