package io.hieu.taskmanagementtool.services.impl

import io.hieu.taskmanagementtool.domain.User
import io.hieu.taskmanagementtool.exceptions.UsernameAlreadyExistsException
import io.hieu.taskmanagementtool.repositories.UserRepository
import io.hieu.taskmanagementtool.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl : UserService {
    @Autowired
    private val userRepository: UserRepository? = null

    // Using BCryptPasswordEncoder to encrypt users' passwords that is going to be stored into the database:
    @Autowired
    private val bCryptPasswordEncoder: BCryptPasswordEncoder? = null
    override fun saveUser(newUser: User?): User? {
        return try {
            // Username has to be unique.
            newUser!!.setUsername(newUser.username)
            // Encode newUser's password:
            newUser.setPassword(bCryptPasswordEncoder!!.encode(newUser.password))
            // Make sure that the values of password and confirm password fields match.
            // Don't persist the data of confirm password field:
            newUser.confirmPassword = ""
            userRepository!!.save(newUser)
        } catch (exception: Exception) {
            throw UsernameAlreadyExistsException(String.format("A user with username %s already exists in the database!", newUser!!.username))
        }
    }
}