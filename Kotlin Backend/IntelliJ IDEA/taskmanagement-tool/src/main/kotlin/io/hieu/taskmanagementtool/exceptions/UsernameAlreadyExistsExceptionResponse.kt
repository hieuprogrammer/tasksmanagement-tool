package io.hieu.taskmanagementtool.exceptions

class UsernameAlreadyExistsExceptionResponse(var username: String)