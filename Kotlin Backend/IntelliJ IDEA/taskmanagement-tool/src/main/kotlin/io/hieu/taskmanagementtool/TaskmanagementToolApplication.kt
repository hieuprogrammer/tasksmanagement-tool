package io.hieu.taskmanagementtool

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@SpringBootApplication
class TaskmanagementToolApplication {
    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(TaskmanagementToolApplication::class.java, *args)
        }

        // For DEBUGGING purpose:
//        @JvmStatic
//        fun main(args: Array<String>) {
//            val applicationContext: ApplicationContext = SpringApplication.run(TaskmanagementToolApplication::class.java, *args)
//            println("All Spring beans in the IoC Container:")
//            val beans = applicationContext.beanDefinitionNames
//            for (bean in beans) {
//                println(String.format(" - %s", bean))
//            }
//        }
    }
}