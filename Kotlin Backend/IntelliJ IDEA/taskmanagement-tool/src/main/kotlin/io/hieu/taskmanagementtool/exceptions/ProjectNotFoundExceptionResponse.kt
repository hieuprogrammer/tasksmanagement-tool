package io.hieu.taskmanagementtool.exceptions

class ProjectNotFoundExceptionResponse(var projectIdAsString: String)