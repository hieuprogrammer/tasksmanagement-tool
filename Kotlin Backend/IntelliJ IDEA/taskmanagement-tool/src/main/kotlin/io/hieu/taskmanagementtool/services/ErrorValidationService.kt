package io.hieu.taskmanagementtool.services

import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult

interface ErrorValidationService {
    fun ErrorValidationService(bindingResult: BindingResult?): ResponseEntity<*>?
}