package io.hieu.taskmanagementtool.security

import io.hieu.taskmanagementtool.security.SecurityConstants.HTTP_HEADER_STRING
import io.hieu.taskmanagementtool.security.SecurityConstants.JWT_PREFIX
import io.hieu.taskmanagementtool.services.CustomUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JsonWebTokenAuthenticationFilter : OncePerRequestFilter() {
    @Autowired
    private val jsonWebTokenProvider: JsonWebTokenProvider? = null

    @Autowired
    private val customUserDetailsService: CustomUserDetailsService? = null

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(
            httpServletRequest: HttpServletRequest,
            httpServletResponse: HttpServletResponse,
            filterChain: FilterChain) {
        try {
            val jsonWebToken = getJsonWebTokenFromRequest(httpServletRequest)
            if (StringUtils.hasText(jsonWebToken) && jsonWebTokenProvider!!.validateJSONWebToken(jsonWebToken)) {
                val userId = jsonWebTokenProvider.getUserIdFromJsonWebToken(jsonWebToken)
                val userDetails = customUserDetailsService!!.loadUserById(userId)
                val authentication = UsernamePasswordAuthenticationToken(
                        userDetails, null, emptyList())
                authentication.details = WebAuthenticationDetailsSource().buildDetails(httpServletRequest)
                SecurityContextHolder.getContext().authentication = authentication
            }
        } catch (exception: Exception) {
            logger.error("Could not set user authentication in Spring security context!" + exception.message)
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse)
    }

    private fun getJsonWebTokenFromRequest(httpServletRequest: HttpServletRequest): String? {
        val bearerJsonWebToken = httpServletRequest.getHeader(HTTP_HEADER_STRING)
        return if (StringUtils.hasText(bearerJsonWebToken) && bearerJsonWebToken.startsWith(JWT_PREFIX)) {
            bearerJsonWebToken.substring(7, bearerJsonWebToken.length)
        } else null
    }
}