package io.hieu.taskmanagementtool.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ProjectDescriptionException(message: String?) : RuntimeException(message)