package io.hieu.taskmanagementtool.payload

import javax.validation.constraints.NotBlank

class LoginRequest {
    var username: @NotBlank(message = "Username is required and can not be blank!") String? = null
    var password: @NotBlank(message = "Password is required and can not be blank!") String? = null
}