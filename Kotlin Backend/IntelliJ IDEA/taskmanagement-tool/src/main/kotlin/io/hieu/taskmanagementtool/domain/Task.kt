package io.hieu.taskmanagementtool.domain

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank

@Entity
class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(updatable = false, unique = true)
    var projectSequence: String? = null
    var summary: @NotBlank(message = "Project summary can not be blank!") String? = null
    var acceptanceCriteria: String? = null
    var status: String? = null
    var priority: Int? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var dueDate: Date? = null

    // JPA's ManyToOne relationship mapping with Backlog:
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "backlog_id", updatable = false, nullable = false)
    @JsonIgnore
    var backlog: Backlog? = null

    @Column(updatable = false)
    var projectIdentifier: String? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var createAt: Date? = null

    @JsonFormat(pattern = "yyyy-mm-dd")
    var updateAt: Date? = null

    @PrePersist
    protected fun onCreate() {
        createAt = Date()
    }

    @PreUpdate
    protected fun onUpdate() {
        updateAt = Date()
    }

    override fun toString(): String {
        return "Task{" +
                "id=" + id +
                ", projectSequence='" + projectSequence + '\'' +
                ", summary='" + summary + '\'' +
                ", acceptanceCriteria='" + acceptanceCriteria + '\'' +
                ", status='" + status + '\'' +
                ", priority=" + priority +
                ", dueDate=" + dueDate +
                ", projectIdentifier='" + projectIdentifier + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                '}'
    }
}