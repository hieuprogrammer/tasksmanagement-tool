package io.hieu.taskmanagementtool.repositories

import io.hieu.taskmanagementtool.domain.Backlog
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface BacklogRepository : CrudRepository<Backlog?, Long?> {
    fun findByProjectIdentifier(identifier: String?): Backlog?
}