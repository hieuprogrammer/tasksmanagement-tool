package io.hieu.taskmanagementtool.payload

class LoginSuccessJWTResponse {
    var isSuccess = false
    var jsonWebToken: String? = null

    constructor() {}
    constructor(success: Boolean, jsonWebToken: String?) {
        isSuccess = success
        this.jsonWebToken = jsonWebToken
    }

    override fun toString(): String {
        return "LoginSuccessJWTResponse{" +
                "success=" + isSuccess +
                ", jsonWebToken='" + jsonWebToken + '\'' +
                '}'
    }
}