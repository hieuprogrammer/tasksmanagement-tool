package io.hieu.taskmanagementtool.security.validator

import io.hieu.taskmanagementtool.domain.User
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator

@Component
class UserValidator : Validator {
    override fun supports(aClass: Class<*>): Boolean {
        return User::class.java == aClass
    }

    override fun validate(`object`: Any, errors: Errors) {
        val user = `object` as User
        if (user.password.length < 6) {
            errors.rejectValue("password", "Length", "Password length must be at least 6 characters long!")
        }
        if (!user.password.equals(user.confirmPassword)) {
            errors.rejectValue("password", "Match", "Password and password confirmation did not match!")
        }
    }
}