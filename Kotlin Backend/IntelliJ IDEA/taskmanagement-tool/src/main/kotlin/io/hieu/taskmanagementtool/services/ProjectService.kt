package io.hieu.taskmanagementtool.services

import io.hieu.taskmanagementtool.domain.Project

interface ProjectService {
    fun saveOrUpdateProject(project: Project?, username: String?): Project?

    fun findAllProjects(username: String?): Iterable<Project?>?

    fun findById(id: Long?): Project?

    fun findByIdentifier(identifier: String?, username: String?): Project?

    fun findByName(projectName: String?): Project?

    fun findByDescription(description: String?): Project?

    fun delete(id: Long?)

    fun deleteByIdentifier(identifier: String?, username: String?)

    fun deleteByProjectName(projectName: String?)

    fun deleteByDescription(description: String?)
}