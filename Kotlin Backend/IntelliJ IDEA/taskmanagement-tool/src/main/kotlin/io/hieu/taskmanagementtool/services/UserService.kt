package io.hieu.taskmanagementtool.services

import io.hieu.taskmanagementtool.domain.User

interface UserService {
    fun saveUser(newUser: User?): User?
}