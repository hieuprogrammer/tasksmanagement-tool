-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: taskmanagement-tool
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `backlog`
--

DROP TABLE IF EXISTS `backlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `backlog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_identifier` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `project_task_sequence` int(11) DEFAULT NULL,
  `project_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl9uchve1jja83kpywpr72gi8k` (`project_id`),
  CONSTRAINT `FKl9uchve1jja83kpywpr72gi8k` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `backlog`
--

LOCK TABLES `backlog` WRITE;
/*!40000 ALTER TABLE `backlog` DISABLE KEYS */;
INSERT INTO `backlog` VALUES (1,'JAVA',5,1),(2,'PYTH',0,2),(4,'KOTL',0,5),(5,'PYTHN',0,6);
/*!40000 ALTER TABLE `backlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `project_identifier` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `project_leader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `project_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nh7jg4qyw1dm5y0bn2vwoq6v2` (`project_identifier`),
  KEY `FKo06v2e9kuapcugnyhttqa1vpt` (`user_id`),
  CONSTRAINT `FKo06v2e9kuapcugnyhttqa1vpt` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'2020-06-19 01:53:15.629000','Java learning roadmap.','2020-01-19 07:06:00.000000','JAVA','hieucoder@outlook.com','Learn Java','2020-01-19 07:06:00.000000',NULL,1),(2,'2020-06-19 02:40:31.706000','Python learning roadmap.','2020-01-19 07:06:00.000000','PYTH','hieu.minhle@nashtechglobal.com','Learn Python','2020-01-19 07:06:00.000000',NULL,2),(5,'2020-06-19 02:41:59.232000','Kotlin learning roadmap.','2020-01-19 07:06:00.000000','KOTL','hieucoder@outlook.com','Learn Kotlin','2020-01-19 07:06:00.000000',NULL,1),(6,'2020-06-19 02:42:24.661000','Python learning roadmap.','2020-01-19 07:06:00.000000','PYTHN','hieucoder@outlook.com','Learn Python','2020-01-19 07:06:00.000000',NULL,1);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acceptance_criteria` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_at` datetime(6) DEFAULT NULL,
  `due_date` datetime(6) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `project_identifier` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `project_sequence` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `summary` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `update_at` datetime(6) DEFAULT NULL,
  `backlog_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6i417n3gverliloh2a7oxxwn` (`project_sequence`),
  KEY `FKqfhsv2jylwq57wt13myy1kq7j` (`backlog_id`),
  CONSTRAINT `FKqfhsv2jylwq57wt13myy1kq7j` FOREIGN KEY (`backlog_id`) REFERENCES `backlog` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,'Learn the fundamentals of Java SE including its syntax, data structures, and Java SE frameworks including Java Swing, JavaFX, and main differences between different Java versions.','2020-06-19 02:20:46.646000','2020-06-19 07:00:00.000000',1,'JAVA','JAVA - 1','DONE','Learn Java SE','2020-06-19 02:30:02.210000',1),(2,'Learn the fundamentals of what the Internet is, what a website is, the Client-Server architecture, HTTP Protocol version 1.1, Java Servlet, Java Server Pages, HTML, CSS, Javascript. Make a simple website with hard-coded data and test it.','2020-06-19 02:26:06.674000','2020-06-19 07:00:00.000000',2,'JAVA','JAVA - 2','IN_PROGRESS','Learn Java EE','2020-06-19 02:30:31.692000',1),(3,'Learn the fundamentals of what is data, what is a database, the use of Structured Query Language (SQL) to interact with data in relational databases.','2020-06-19 02:29:37.274000','2020-06-19 07:00:00.000000',3,'JAVA','JAVA - 3','TO_DO','Learn Structured Query Language (SQL)','2020-06-19 02:30:49.809000',1),(4,'Learn the fundamentals concepts of HTML, CSS, Javascript including how HTML code structure web pages, its previous versions, and how they compare to HTML5. Make a simple website using HTML5, CSS3, Javascript ES6, and test running it on different browsers.','2020-06-19 02:35:23.076000','2020-06-19 07:00:00.000000',2,'JAVA','JAVA - 4','IN_PROGRESS','Learn HTML5, CSS3, Javascript ES6',NULL,1),(5,'Learn the fundamental concepts of Java EE\'s Spring Framework, make a simple website using MySQL, Spring Framework, HTML5, CSS3, Javascript ES6, and test it.','2020-06-19 02:37:19.242000','2020-06-19 07:00:00.000000',1,'JAVA','JAVA - 5','DONE','Learn Java EE\'s Spring Framework','2020-06-19 02:37:47.776000',1);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` datetime(6) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `update_at` datetime(6) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2020-06-19 01:52:15.085000','Hieu Minh Le','$2a$10$LUZdEc20x7fHYm50NleFUudmnqiaY5smRtxCyojvZ4muha5EM07bm',NULL,'hieucoder@outlook.com'),(2,'2020-06-19 02:39:23.116000','Lê Minh Hiếu','$2a$10$NeOW3OBMsyntFqCLWh3t8OYiTZ7Tu3ZRIDkjp4H4r9pC.lvD8mRve',NULL,'hieu.minhle@nashtechglobal.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-19  3:34:12
