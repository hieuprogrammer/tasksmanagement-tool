package io.hieu.taskmanagementtool.payload;

public class LoginSuccessJWTResponse {
    private boolean success;
    private String jsonWebToken;

    public LoginSuccessJWTResponse() {
    }

    public LoginSuccessJWTResponse(boolean success, String jsonWebToken) {
        this.success = success;
        this.jsonWebToken = jsonWebToken;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getJsonWebToken() {
        return jsonWebToken;
    }

    public void setJsonWebToken(String jsonWebToken) {
        this.jsonWebToken = jsonWebToken;
    }

    @Override
    public String toString() {
        return "LoginSuccessJWTResponse{" +
                "success=" + success +
                ", jsonWebToken='" + jsonWebToken + '\'' +
                '}';
    }
}