package io.hieu.taskmanagementtool.exceptions;

public class ProjectDescriptionExceptionResponse {
    private String projectDescription;

    public ProjectDescriptionExceptionResponse(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }
}