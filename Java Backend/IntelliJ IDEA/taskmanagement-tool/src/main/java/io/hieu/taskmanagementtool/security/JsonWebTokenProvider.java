package io.hieu.taskmanagementtool.security;

import io.hieu.taskmanagementtool.domain.User;
import io.jsonwebtoken.*;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static io.hieu.taskmanagementtool.security.SecurityConstants.JWT_EXPIRATION_TIME;
import static io.hieu.taskmanagementtool.security.SecurityConstants.JWT_SECRET_KEY;

@Component
public class JsonWebTokenProvider {
    // Generate the JSON Web Token:
    public String generateJWT(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        Date now = new Date(System.currentTimeMillis());

        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION_TIME);

        String userId = Long.toString(user.getId());

        Map<String, Object> claims = new HashMap<>();
        claims.put("id", (Long.toString(user.getId())));
        claims.put("username", user.getUsername());
        claims.put("fullName", user.getFullName());

        return Jwts.builder()
                .setSubject(userId)
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET_KEY)
                .compact();
    }

    // Validate the JSON Web Token:
    public boolean validateJSONWebToken(String jsonWebToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET_KEY).parseClaimsJws(jsonWebToken);
            return true;
        } catch (SignatureException exception) {
            java.lang.System.out.println("Invalid JSON Web Token signature!");
        } catch (MalformedJwtException exception) {
            java.lang.System.out.println("Invalid JSON Web Token!");
        } catch (ExpiredJwtException exception) {
            java.lang.System.out.println("Expired JSON Web Token!");
        } catch (UnsupportedJwtException exception) {
            java.lang.System.out.println("Unsupported JSON Web Token!");
        } catch (IllegalArgumentException exception) {
            java.lang.System.out.println("JSON Web Token claims string is empty!");
        }
        return false;
    }

    // Get user ID from JSON Web Token:
    public Long getUserIdFromJsonWebToken(String jsonWebToken) {
        Claims claims = Jwts.parser().setSigningKey(JWT_SECRET_KEY).parseClaimsJws(jsonWebToken).getBody();
        String id = (String) claims.get("id");
        return Long.parseLong(id);
    }
}