package io.hieu.taskmanagementtool.exceptions;

public class ProjectNotFoundExceptionResponse {
    private String projectIdAsString;

    public ProjectNotFoundExceptionResponse(String projectIdAsString) {
        this.projectIdAsString = projectIdAsString;
    }

    public String getProjectIdAsString() {
        return projectIdAsString;
    }

    public void setProjectIdAsString(String projectIdAsString) {
        this.projectIdAsString = projectIdAsString;
    }
}