package io.hieu.taskmanagementtool.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Backlog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer projectTaskSequence = 0;
    private String projectIdentifier;

    // JPA's OneToOne relationship mapping with Project entity:
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonIgnore // Adding this @JsonIgnore annotation on the child side of the JPA relationship will solve the recursive issue which would lead to a Stack Overflow error.
    private Project project;

    // JPA's OneToMany relationship mapping with Task entity:
    @OneToMany(
            cascade = CascadeType.REFRESH, // When deleting a Task that belongs to a Backlog object, JPA refreshes the Backlog data and save the new Backlog data to the database without the just deleted Task data that is no longer exists.
            fetch = FetchType.EAGER, // EAGER-Fetching all Task(s) data when this Backlog object is called.
            mappedBy = "backlog",
            orphanRemoval = true // orphanRemoval attribute: When the child entity is no longer referenced from the parent, get rid of the child.
    )
    private List<Task> tasks = new ArrayList<>();

    public Backlog() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProjectTaskSequence() {
        return projectTaskSequence;
    }

    public void setProjectTaskSequence(Integer projectTaskSequence) {
        this.projectTaskSequence = projectTaskSequence;
    }

    public String getProjectIdentifier() {
        return projectIdentifier;
    }

    public void setProjectIdentifier(String projectIdentifier) {
        this.projectIdentifier = projectIdentifier;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}