package io.hieu.taskmanagementtool.repositories;

import io.hieu.taskmanagementtool.domain.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findByProjectIdentifierOrderByPriority(String id);

    Task findByProjectSequence(String sequence);
}