package io.hieu.taskmanagementtool.web;

import io.hieu.taskmanagementtool.domain.User;
import io.hieu.taskmanagementtool.payload.LoginRequest;
import io.hieu.taskmanagementtool.payload.LoginSuccessJWTResponse;
import io.hieu.taskmanagementtool.security.JsonWebTokenProvider;
import io.hieu.taskmanagementtool.services.ErrorValidationService;
import io.hieu.taskmanagementtool.services.UserService;
import io.hieu.taskmanagementtool.security.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.hieu.taskmanagementtool.security.SecurityConstants.JWT_PREFIX;

@RestController
@RequestMapping(path = {"/api/users", "api/users.html", "api/users/index", "api/users/index.html"})
public class UserController {
    @Autowired
    private ErrorValidationService errorValidationService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private JsonWebTokenProvider jsonWebTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping(path = {"/login", "/login/"})
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, BindingResult bindingResult) {
        ResponseEntity<?> errors = errorValidationService.ErrorValidationService(bindingResult);
        if (errors != null) {
            return errors;
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jsonWebToken = JWT_PREFIX + jsonWebTokenProvider.generateJWT(authentication);

        return ResponseEntity.ok(new LoginSuccessJWTResponse(true, jsonWebToken));
    }

    @PostMapping(path = {"/register", "/register/"})
    public ResponseEntity<?> registerUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        // Validate the values of password and confirm password fields match:
        userValidator.validate(user, bindingResult);
        ResponseEntity<?> errors = errorValidationService.ErrorValidationService(bindingResult);
        if (errors != null) {
            return errors;
        }
        User newUser = userService.saveUser(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }
}