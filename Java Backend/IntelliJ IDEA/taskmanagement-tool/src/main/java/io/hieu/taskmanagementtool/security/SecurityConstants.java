package io.hieu.taskmanagementtool.security;

public class SecurityConstants {
    public static final String SIGN_UP_URLS = "/api/users/**";
    public static final String H2_URL = "h2-console/**";
    public static final String JWT_SECRET_KEY = "SecretKeyToGenerateJWTs";
    public static final String JWT_PREFIX = "Bearer ";
    public static final String HTTP_HEADER_STRING = "Authorization";
    public static final long JWT_EXPIRATION_TIME = 30_000; // 30 seconds.
}