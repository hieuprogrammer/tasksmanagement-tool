package io.hieu.taskmanagementtool.services.impl;

import io.hieu.taskmanagementtool.domain.User;
import io.hieu.taskmanagementtool.exceptions.UsernameAlreadyExistsException;
import io.hieu.taskmanagementtool.repositories.UserRepository;
import io.hieu.taskmanagementtool.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    // Using BCryptPasswordEncoder to encrypt users' passwords that is going to be stored into the database:
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User saveUser(User newUser) {
        try {
            // Username has to be unique.
            newUser.setUsername(newUser.getUsername());
            // Encode newUser's password:
            newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
            // Make sure that the values of password and confirm password fields match.
            // Don't persist the data of confirm password field:
            newUser.setConfirmPassword("");
            return userRepository.save(newUser);
        } catch (Exception exception) {
            throw new UsernameAlreadyExistsException(String.format("A user with username %s already exists in the database!", newUser.getUsername()));
        }
    }
}