package io.hieu.taskmanagementtool.services.impl;

import io.hieu.taskmanagementtool.domain.User;
import io.hieu.taskmanagementtool.repositories.UserRepository;
import io.hieu.taskmanagementtool.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            new UsernameNotFoundException(String.format("User with username %s is not found in the database!", username));
        }
        return user;
    }

    @Transactional
    @Override
    public User loadUserById(Long id) {
        User user = userRepository.getById(id);
        if (user == null) {
            new UsernameNotFoundException(String.format("User with ID number %d not found in the database!", id));
        }
        return user;
    }
}