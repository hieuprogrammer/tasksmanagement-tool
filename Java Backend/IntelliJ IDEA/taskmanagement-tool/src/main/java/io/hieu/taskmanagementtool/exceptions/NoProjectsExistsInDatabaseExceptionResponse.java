package io.hieu.taskmanagementtool.exceptions;

public class NoProjectsExistsInDatabaseExceptionResponse {
    private String noProjectsExistInDatabaseMessage;

    public NoProjectsExistsInDatabaseExceptionResponse(String noProjectsExistInDatabaseMessage) {
        this.noProjectsExistInDatabaseMessage = noProjectsExistInDatabaseMessage;
    }

    public String getNoProjectsExistInDatabaseMessage() {
        return noProjectsExistInDatabaseMessage;
    }

    public void setNoProjectsExistInDatabaseMessage(String noProjectsExistInDatabaseMessage) {
        this.noProjectsExistInDatabaseMessage = noProjectsExistInDatabaseMessage;
    }
}