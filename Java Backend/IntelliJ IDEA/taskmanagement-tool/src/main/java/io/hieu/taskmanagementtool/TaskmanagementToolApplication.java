package io.hieu.taskmanagementtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class TaskmanagementToolApplication {
    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    public static void main(String[] args) {
//        SpringApplication.run(TaskmanagementToolApplication.class, args);
//    }

    // For DEBUGGING purpose:
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(TaskmanagementToolApplication.class, args);

        java.lang.System.out.println("All Spring beans in the IoC Container:");
        String beans[] = applicationContext.getBeanDefinitionNames();
        for (String bean : beans) {
            java.lang.System.out.println(String.format(" - %s", bean));
        }
    }
}