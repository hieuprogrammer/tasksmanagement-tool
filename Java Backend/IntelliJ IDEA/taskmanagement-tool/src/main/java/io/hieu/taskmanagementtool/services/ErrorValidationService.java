package io.hieu.taskmanagementtool.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

public interface ErrorValidationService {
    ResponseEntity<?> ErrorValidationService(BindingResult bindingResult);
}