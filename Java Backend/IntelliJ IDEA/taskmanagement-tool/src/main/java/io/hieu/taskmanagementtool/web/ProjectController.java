package io.hieu.taskmanagementtool.web;

import io.hieu.taskmanagementtool.domain.Project;
import io.hieu.taskmanagementtool.services.ErrorValidationService;
import io.hieu.taskmanagementtool.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(path = {"/api/projects", "/api/projects.html", "/api/projects/index", "/api/projects/index.html"})
@CrossOrigin
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ErrorValidationService errorValidationService;

    // HTTP 1.1 POST request(s):
    @PostMapping(path = {"", "/**", "/index", "/index.html"})
    public ResponseEntity<?> createNewProject(
            @Valid @RequestBody Project project,
            BindingResult bindingResult,
            Principal principal) {
        ResponseEntity<?> errors = errorValidationService.ErrorValidationService(bindingResult);
        if (errors != null) {
            return errors;
        } else {
            Project newProject = projectService.saveOrUpdateProject(project, principal.getName());
            java.lang.System.out.println(String.format("Successfully created and stored a new project named \"%s\" to the database!", project.getProjectName()));
            return new ResponseEntity<>(newProject, HttpStatus.CREATED);
        }
    }

    // HTTP 1.1 GET request(s):
    @GetMapping(path = {"", "/", "/all", "/all/", "/all/index", "/all/index/", "/all/index.html", "/all/index.html/"})
    public Iterable<Project> getAllProjects(Principal principal) {
        java.lang.System.out.println(String.format("Retrieved all projects from the database!"));
        return projectService.findAllProjects(principal.getName());
    }

    @GetMapping(path = {"/{project_id}", "/{project_id}/", "/{project_id}/index", "/{project_id}/index/", "/{project_id}/index.html", "/{project_id}/index.html/"})
    public ResponseEntity<?> getProjectById(@PathVariable Long project_id) {
        Project project = projectService.findById(project_id);
        java.lang.System.out.println(String.format("Project with ID number %d retrieved from the database!", project_id));
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(path = {"/get-project-by-identifier/{project_identifier}", "/get-project-by-identifier/{project_identifier}/", "/get-project-by-identifier/{project_identifier}/index", "/get-project-by-identifier/{project_identifier}/index/", "/get-project-by-identifier/{project_identifier}/index.html", "/get-project-by-identifier/{project_identifier}/index.html/"})
    public ResponseEntity<?> getProjectByIdentifier(@PathVariable String project_identifier, Principal principal) {
        Project project = projectService.findByIdentifier(project_identifier, principal.getName());
        java.lang.System.out.println(String.format("Project with identifier \"%s\" retrieved from the database!", project_identifier.toUpperCase()));
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @GetMapping(path = {"/get-project-by-name/{project_name}", "/get-project-by-name/{project_name}/", "/get-project-by-name/{project_name}/index", "/get-project-by-name/{project_name}/index/", "/get-project-by-name/{project_name}/index.html", "/get-project-by-name/{project_name}/index.html/"})
    public ResponseEntity<?> getProjectByName(@PathVariable String project_name) {
        Project project = projectService.findByName(project_name);
        java.lang.System.out.println(String.format("Project with name \"%s\" is retrieved from the database!", project_name));
        return new ResponseEntity<>(project, HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = {"/get-project-by-description/{project_description}", "/get-project-by-description/{project_description}/", "/get-project-by-description/{project_description}/index", "/get-project-by-description/{project_description}/index/", "/get-project-by-description/{project_description}/index.html", "/get-project-by-description/{project_description}/index.html/"})
    public ResponseEntity<?> getProjectByDescription(@PathVariable String project_description) {
        Project project = projectService.findByDescription(project_description);
        java.lang.System.out.println(String.format("Project with description \"%s\" is retrieved from the database!", project_description));
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    // HTTP 1.1 DELETE request(s):
    @DeleteMapping(path = {"/{project_id}", "/{project_id}/", "/{project_id}/index", "/{project_id}/index/", "/{project_id}/index.html", "/{project_id}/index.html/"})
    public ResponseEntity<?> deleteProject(@PathVariable Long project_id) {
        projectService.delete(project_id);
        java.lang.System.out.println(String.format("Project with ID number %d is removed from the database!", project_id));
        return new ResponseEntity<>(
                String.format("Project with ID number %d is removed from the database!", project_id), HttpStatus.OK);
    }

    @DeleteMapping(path = {"/delete-project-by-identifier/{project_identifier}", "/delete-project-by-identifier/{project_identifier}/", "/delete-project-by-identifier/{project_identifier}/index", "/delete-project-by-identifier/{project_identifier}/index/", "/delete-project-by-identifier/{project_identifier}/index.html", "/delete-project-by-identifier/{project_identifier}/index.html/"})
    public ResponseEntity<?> deleteProjectByIdentifier(@PathVariable String project_identifier, Principal principal) {
        projectService.deleteByIdentifier(project_identifier, principal.getName());
        java.lang.System.out.println(String.format("Project with identifier \"%s\" is removed from the database!", project_identifier.toUpperCase()));
        return new ResponseEntity<>(String.format("Project with identifier \"%s\" is removed from the database!", project_identifier.toUpperCase()), HttpStatus.OK);
    }

    @DeleteMapping(path = {"/delete-project-by-name/{project_name}", "/delete-project-by-name/{project_name}/", "/delete-project-by-name/{project_name}/index", "/delete-project-by-name/{project_name}/index/", "/delete-project-by-name/{project_name}/index.html", "/delete-project-by-name/{project_name}/index.html/"})
    public ResponseEntity<?> deleteProjectByName(@PathVariable String project_name) {
        projectService.deleteByProjectName(project_name);
        java.lang.System.out.println(String.format("Project with project name: \"%s\" is removed from the database!", project_name));
        return new ResponseEntity<>(String.format("Project with project name: \"%s\" is removed from the database!", project_name), HttpStatus.OK);
    }

    @DeleteMapping(path = {"/delete-project-by-description/{project_description}", "/delete-project-by-description/{project_description}/", "/delete-project-by-description/{project_description}/index", "/delete-project-by-description/{project_description}/index/", "/delete-project-by-description/{project_description}/index.html", "/delete-project-by-description/{project_description}/index.html/"})
    public ResponseEntity<?> deleteProjectByDescription(@PathVariable String project_description) {
        projectService.deleteByDescription(project_description);
        java.lang.System.out.println(String.format("Project with description: \"%s\" is removed from the database!", project_description));
        return new ResponseEntity<>(String.format("Project with description: \"%s\" is removed from the database!", project_description), HttpStatus.OK);
    }
}