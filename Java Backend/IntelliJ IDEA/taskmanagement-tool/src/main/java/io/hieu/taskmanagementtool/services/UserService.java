package io.hieu.taskmanagementtool.services;

import io.hieu.taskmanagementtool.domain.User;

public interface UserService {
    User saveUser(User newUser);
}