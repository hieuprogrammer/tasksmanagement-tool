package io.hieu.taskmanagementtool.services;

import io.hieu.taskmanagementtool.domain.Project;

public interface ProjectService {
    Project saveOrUpdateProject(Project project, String username);

    Iterable<Project> findAllProjects(String username);

    Project findById(Long id);

    Project findByIdentifier(String identifier, String username);

    Project findByName(String projectName);

    Project findByDescription(String description);

    void delete(Long id);

    void deleteByIdentifier(String identifier, String username);

    void deleteByProjectName(String projectName);

    void deleteByDescription(String description);
}