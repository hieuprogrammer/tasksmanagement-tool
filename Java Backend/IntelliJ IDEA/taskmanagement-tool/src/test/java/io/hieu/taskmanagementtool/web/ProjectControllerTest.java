package io.hieu.taskmanagementtool.web;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectControllerTest {
    @Test
    void createNewProject() {
    }

    @Test
    void getAllProjects() {
    }

    @Test
    void getProjectById() {
    }

    @Test
    void getProjectByIdentifier() {
    }

    @Test
    void getProjectByName() {
    }

    @Test
    void getProjectByDescription() {
    }

    @Test
    void deleteProject() {
    }

    @Test
    void deleteProjectByIdentifier() {
    }

    @Test
    void deleteProjectByName() {
    }

    @Test
    void deleteProjectByDescription() {
    }
}