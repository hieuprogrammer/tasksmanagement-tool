package io.hieu.taskmanagementtool.services.impl;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectServiceImplTest {
    @Test
    void saveOrUpdateProject() {
    }

    @Test
    void findAllProjects() {
    }

    @Test
    void findById() {
    }

    @Test
    void findByIdentifier() {
    }

    @Test
    void findByName() {
    }

    @Test
    void findByDescription() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteByIdentifier() {
    }

    @Test
    void deleteByProjectName() {
    }

    @Test
    void deleteByDescription() {
    }
}